# Proyectos AT89C51 hardware

Colección de proyectos con el microcontrolador AT89C51.

Los proyectos que contiene este repositorio son los soguientes:

Placas de desarrollo con el microcontrolador:
* Básica con los componentes necesarios para hacer funcionar el uControlador.
* 4x 7 Segmentos + botones y leds.
* Drivers de motores paso a paso x3
